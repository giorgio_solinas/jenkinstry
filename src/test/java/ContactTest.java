import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;
//import org.junit.jupiter.params.shadow.com.univocity.parsers.annotations.Nested;
// this import create a problem while running the test

import static org.junit.jupiter.api.Assertions.*;

class ContactTest {
    public Contact mycontact;

    @BeforeEach
    void init() {
        System.out.println("Initialisation");
        mycontact = new Contact("Giorgio", "giorgio@email.it", 123);
    }


    @Test
    @DisplayName("Test for all get methods")
    void verifyGet() {
        System.out.println("Testing get methods");
        assertAll(
                () -> assertEquals("Giorgio", mycontact.getName()),
                () -> assertEquals("giorgio@email.it", mycontact.getEmail()),
                () -> assertEquals(123, mycontact.getId()),
                () -> assertNotSame(new Contact("Giorgio", "giorgio@email.it", 123), mycontact));
    }

   /* @Test
    void getEmail() {
        System.out.println("Testing getemail");
        assertEquals("giorgio@email.it", mycontact.getEmail());
    }

    @Test
    void getId() {
        System.out.println("Testing getid");
        assertEquals(123, mycontact.getId());
    }*/

    @Nested
    @DisplayName("Test method for setter: ")
    class testSetters {
        @Test
        @DisplayName("name")
        void setName () {
            mycontact.setName("Merda");
            assertEquals("Merda", mycontact.getName());
        }


        @Test
        @DisplayName("email")
        void setEmail () {
            mycontact.setEmail("merda@email.com");
            assertEquals("merda@email.com", mycontact.getEmail());
        }

        @Test
        @DisplayName("id")
        void setId () {
            mycontact.setId(321);
            assertEquals(321, mycontact.getId());
        }
    }
}