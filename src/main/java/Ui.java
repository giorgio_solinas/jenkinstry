public class Ui {
    public static void view(){
        System.out.println("1: See contacts");
        System.out.println("2: Add a new contact");
        System.out.println("3: Remove an existing contact");
        System.out.println("4: Modify an existing contact");
        System.out.println("x: Exit");
    }
}
