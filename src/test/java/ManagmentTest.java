import jdk.jfr.Name;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class ManagmentTest {
    private static Contact c1;
    private static Contact c2;
    private static ArrayList<Contact> contactList = new ArrayList();

    @BeforeAll
    static void loadInit(){
        Managment.load();
        //contactList = Managment.contactList;
    }
    @BeforeEach
    void init() {
        c1 = new Contact("culo", "tette@email.com", Managment.contactList.size() + 1);

    }

    @Test
    void testLoad() {
        System.out.println("Load test");
        assertNotNull(Managment.contactList);
        assertEquals(2, Managment.contactList.size());
        assertSame(Managment.c1, Managment.contactList.get(0));
        assertSame(Managment.c2, Managment.contactList.get(1));
        //assertIterableEquals(Managment.contactList,this.contactList);
    }

    @Test
    @Disabled
    void testViewContacts() {
        assertEquals("Contact{name='a', email='s', id=1}", String.valueOf(Managment.contactList.get(0)));
        assertEquals("Contact{name='q', email='w', id=2}", String.valueOf(Managment.contactList.get(1)));
        //assertEquals(String.valueOf(Managment.contactList.get(1)), String.valueOf(this.contactList.get(1)));
    }

    @Test
    void testAddContact() {
        System.out.println(c1);
        Managment.addContact(c1);
        assertAll(
                () -> assertEquals(3, Managment.contactList.size()),
                () -> assertEquals("culo", Managment.contactList.get(2).getName()),
                () -> assertEquals("tette@email.com", Managment.contactList.get(2).getEmail()),
                () -> assertEquals(3, Managment.contactList.get(2).getId())
        );
        assertSame(c1, Managment.contactList.get(2));

    }

    @Test
    void removeContact() {
        for(Contact c : Managment.contactList){
            System.out.println(c);
        }
        c1.setName("a"); c1.setEmail("s");c1.setId(1);
        Managment.removeContact(c1);
        assertAll(
                () -> assertEquals(2, Managment.contactList.size()),
                () -> assertEquals("q", Managment.contactList.get(0).getName()),
                () -> assertEquals("w", Managment.contactList.get(0).getEmail()),
                () -> assertEquals(2, Managment.contactList.get(0).getId())
        );

    }

   /* @Test
    void modifyContact() {
        Managment.modifyContact(Managment.contactList.get(0));
    }*/
}