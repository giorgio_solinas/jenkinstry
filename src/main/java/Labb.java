import java.util.Scanner;

public class Labb {
    public static Contact c;
    public static void main(String[] args) {
        Managment.load();
        Scanner myScanner= new Scanner(System.in);
        String chosenOp="";
        while(!chosenOp.equalsIgnoreCase("x")){
            chosenOp=choice(myScanner);
        }
    }

    public static String choice(Scanner scanner) {
        Ui.view();
        String chosen = scanner.next();
        switch (chosen) {
            case "1":
                Managment.viewContacts();
                return chosen;
            case "2":
                c = Managment.searchContact();
                Managment.addContact(c);
                return chosen;
            case "3":
                c= Managment.searchContact();
                Managment.removeContact(c);
                return chosen;
            case "4":
                c= Managment.searchContact();
                Managment.modifyContact(c);
                return chosen;
            default:
                System.out.println("Please insert a correct value");
                return chosen;
        }

    }
}
