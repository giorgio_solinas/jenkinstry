import java.util.ArrayList;
import java.util.Scanner;

public class Managment {
    static ArrayList<Contact> contactList = new ArrayList<>();
    static Scanner myScanner = new Scanner(System.in);
    static Contact contact;
    static Contact c1 = new Contact("a","s",1);
    static Contact c2 = new Contact("q","w",2);
    static boolean found;

    public static void load(){
        contactList.add(c1);
        contactList.add(c2);
    }

    public static void viewContacts() {
        /*String string = "";
        for (Contact c : contactList) {
             string = string + " " + c.toString();
        }
        System.out.println(string);
        return string;*/
        for (Contact c: contactList){
            System.out.println(c);
        }

    }
    public static Contact searchContact(){
        System.out.println("Please insert name and email of the contact");
        System.out.println("Name: ");
        String name = myScanner.nextLine();
        System.out.println("Email: ");
        String email = myScanner.nextLine();
        for (Contact c : contactList) {
            if((name.equalsIgnoreCase(c.getName()))&&(email.equalsIgnoreCase(c.getEmail()))){
                contact = c;
                break;
            }
            else {
                contact = new Contact(name, email, (contactList.size()+1));

            }
        }
        System.out.println(contact);
        return contact;

    }

    public static void addContact(Contact contactPar) {

        for (Contact c : contactList) {
            if ((!contactPar.getName().equalsIgnoreCase(c.getName()))
                    || !(contactPar.getEmail().equalsIgnoreCase(c.getEmail()))) {
                contactList.add(contactPar);
            }
            else{
                System.out.println("Contact already in the list");
            }
            break;
        }
    }


    public static void removeContact(Contact contactPar) {

        for (Contact c : contactList) {
            if (contactPar.getName().equalsIgnoreCase(c.getName()) && contactPar.getEmail().equalsIgnoreCase(c.getEmail())) {
                contactList.remove(c);
            }
            else {
                System.out.println("Contact not found");
            }
            break;
        }

    }

    public static void modifyContact(Contact contactPar) {
        for (Contact c : contactList) {
            if (contactPar.getName().equalsIgnoreCase(c.getName()) && contactPar.getEmail().equalsIgnoreCase(c.getEmail())) {
                System.out.println("What do you want to modify?");
                System.out.println("1: Name");
                System.out.println("2: Email");
                String choice = myScanner.nextLine();

                switch (choice){
                    case "1":{
                        System.out.println("Insert the new name");
                        String newname= myScanner.nextLine();
                        c.setName(newname);
                        break;}
                    case "2":{
                        System.out.println("Insert the new email");
                        String newmail= myScanner.nextLine();
                        c.setEmail(newmail);
                        break;}

                }
            }

        }
    }
}